---
title: AGL Components
---

## Components under development within AGL

### Graphics

- [The AGL compositor](02_agl_compositor.md)
- [DRM lease manager](05_drm_lease_manager.md)


### Sound

- [Pipewire & Wireplumber](07_pipewire_wireplumber.md)
- [IC and Sound Manager](08_Instrument_Cluster_Sound_Management.md)


### Policies

- [Rule based arbitrator](04_Rule_Based_Arbitrator.md)


### Application Lifecycle and Services

- [Application Framework](Application_Framework/01_Introduction.md)

### Application for Demo

- [Momi Navi](Demo_Application/01_Momi_Navi.md)

