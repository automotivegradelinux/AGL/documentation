---
title: IVI Flutter apps
---

# Build and Boot AGL Flutter IVI dashboard demo applications made for GSoC

## 0. Prepare Your Build Host

- Install the required tools to build an AGL Image. For detailed explanation, check [Preparing Your Build host](https://docs.automotivelinux.org/en/master/#01_Getting_Started/02_Building_AGL_Image/02_Preparing_Your_Build_Host/)

## 1. Define Your Top-Level Directory

```bash
$ export AGL_TOP=$HOME/AGL
$ echo 'export AGL_TOP=$HOME/AGL' >> $HOME/.bashrc
$ mkdir -p $AGL_TOP
```

## 2. Download the repo Tool and Set Permissions

```bash
$ mkdir -p $HOME/bin
$ export PATH=$HOME/bin:$PATH
$ echo 'export PATH=$HOME/bin:$PATH' >> $HOME/.bashrc
$ curl https://storage.googleapis.com/git-repo-downloads/repo > $HOME/bin/repo
$ chmod a+x $HOME/bin/repo
```

## 3. Download the AGL Source Files
To download the latest **master** branch  AGL files, use the following commands:
```bash
$ cd $AGL_TOP
$ mkdir master
$ cd master
$ repo init -u https://gerrit.automotivelinux.org/gerrit/AGL/AGL-repo
$ repo sync
```

## 4. Initialize the build environment using aglsetup.sh Script
To initialize the build environment, we must use the setup script.
This script is available here:
```bash
$ $AGL_TOP/master/meta-agl/scripts/aglsetup.sh
```
Run the script:

```bash
$ cd $AGL_TOP/master
$ source meta-agl/scripts/aglsetup.sh -b build-flutter-dashboard -m qemux86-64 agl-demo agl-devel
```

- Here `-b` is used to specify the build directory and `-m` is used to specify the target platform.

- Running this script, will create a build directory if it does not exist. Default build directory: `$AGL_TOP/master/build-flutter-dashboard`
- Default target paltform: `qemux86-64`

## 5. Using BitBake

```bash
$ cd $AGL_TOP/master/build-flutter-dashboard
$ source agl-init-build-env
$ bitbake agl-ivi-demo-flutter
```

## 6. Deploying the AGL Demo Image
Boot the image using QEMU

```bash
$ cd $AGL_TOP/master/build-flutter-dashboard
$ source agl-init-build-env
$ runqemu kvm serialstdio slirp publicvnc
```

## 6. Run the Graphics
To get graphics of the app, you need VNC client like VNC Viewer or Vinagre

- Open the VNC client
- Enter the server address as `localhost:0`

That's it, you should get something like this:
![Screenshot](images/ivi_homescreen.PNG)
